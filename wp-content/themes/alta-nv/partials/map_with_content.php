<?php

  $copy = get_sub_field('copy'); 
  $bg_color = get_sub_field('bg_color'); 
  $map = get_sub_field('google_map'); 
  $switch_columns = get_sub_field('switch_columns'); 
  $expanded_grid = get_sub_field('expanded_grid'); ?>

<section id="section<?= get_row_index(); ?>" class="section map-with-content" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
  <div class="grid-container <?= ($expanded_grid == true && $switch_columns == false ? 'grid-expanded' : 'full'); ?>">
    <div class="grid-x">
      <div class="cell small-12 medium-12 large-6 gmap<?= ($switch_columns == true ? ' reversed' : ''); ?>">
        <div id="copy">
          <?= $map; ?>
        </div> <!-- #copy -->
      </div> <!-- .cell -->

      <div class="cell small-12 medium-12 large-6 copy">
        <div id="copy">
          <?= $copy; ?>
        </div> <!-- #hero -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- section -->