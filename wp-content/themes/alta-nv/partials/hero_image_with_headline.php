<?php

$hero_image = get_sub_field('hero_image');
$headline = get_sub_field('headline'); ?>

<section id="section<?= get_row_index(); ?>" class="section" style="<?= (!empty($hero_image) ? 'background-image:url('.$hero_image["url"].')' : ''); ?>">
  <div id="tagline">
    <?= $headline; ?>
  </div> <!-- #tagline -->

  <div id="hero-nav">
    <?php wp_nav_menu(array('theme_location' => 'menu-hero','container' => '','items_wrap' => '<nav class="%2$s">%3$s</nav>','menu_class' => 'nav nav--hero','depth' => 1)); ?>
  </div> <!-- #hero-nav -->
</section>
