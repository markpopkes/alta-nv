<?php

$headline = get_sub_field('headline'); 
$button_link = get_sub_field('button_link'); 
$button_text = get_sub_field('button_text'); ?>

<section id="section<?= get_row_index(); ?>" class="section call-to-action">
  <div class="grid-container">
    <div class="grid-x">
      <div class="left">
        <?= $headline; ?>
      </div> <!-- .left -->

      <div class="right">
        <div class="alta-button">
          <a href="<?= $button_link['url']; ?>"><?= $button_text; ?></a>
        </div> <!-- .button -->
      </div> <!-- .right --> 

    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>