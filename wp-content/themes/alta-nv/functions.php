<?php

add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
add_post_type_support( 'page', 'excerpt' );

function altanv_register_nav_menus() {
  register_nav_menus(array(
    'menu-hero' => 'Homepage Hero Links',
    'menu-header' => 'Top Navigation Links',
    'menu-nav-links' => 'Popup Menu Navigation',
  ));
} //function
add_action('init', 'altanv_register_nav_menus');


function my_body_classes( $classes ) {
  global $post;
  $current_page = $post->post_name;
  $classes[] = $current_page;
  return $classes;
}
add_filter( 'body_class','my_body_classes' );

// Enqueue CSS styles
function init_enqueue_css() {
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'slick-styles', get_template_directory_uri() . '/dist/slick.css');
	wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), '', null);
}
add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

// Enqueue Admin CSS Styles

function init_enqueue_admin_css() {
  wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/admin/style.css', array(), '', null);
}
add_action( 'admin_enqueue_scripts', 'init_enqueue_admin_css');

// Enqueue Javascript

function init_enqueue_js() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, false, false );
		wp_enqueue_script( 'jquery' );
	}
    wp_enqueue_script( 'tabsjs' , get_stylesheet_directory_uri()  . '/assets/js/vendor/tabs.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/source.js', array( 'jquery' ), '1.2.0', true );

	// Localize template URL for usage in JS
	$data = array(
		'template_dir' => get_stylesheet_directory_uri(),
	);

	// Get locations
	if ( is_page('neighborhood') ) {
        global $post;

        $data['locations'] = get_locations();
    }
	wp_localize_script( 'app', 'AppData', $data );
}
add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );

function init_gmap() { ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAfN1c-tahymsmBJvLXugj5LAooRVyxs4"></script>
	<?php
}
add_action('wp_head', 'init_gmap');

function init_ga_analytics() { ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129351399-6"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-129351399-6');
    </script>
<?php }

add_action( 'wp_head', 'init_ga_analytics' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Alta NV Settings',
		'menu_title'	=> 'Alta NV',
		'menu_slug' 	=> 'altanv-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

require_once( get_template_directory() . '/inc/functions-defaults.php' );

// Keep ACF field metaboxes close by default
function ACF_flexible_content_collapse() { ?>
	<script type="text/javascript">
		jQuery(function($) {
			$('.acf-flexible-content .layout').addClass('-collapsed');
			$('#acf-flexible-content-collapse').detach();
		});
	</script>
	<?php
}
add_action('acf/input/admin_head', 'ACF_flexible_content_collapse');

// Setup Google Map Javascript API Key for use with Advanced Custom Fields
function altanv_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyA9ju5hO9ANZURJ3E-RChL_hx9aj3I6nFU');
}
add_action('acf/init', 'altanv_acf_init');

/*************************************************************/
/*   Friendly Block Titles                                  */
/***********************************************************/

function my_layout_title($title, $field, $layout, $i) {
	if($value = get_sub_field('layout_title')) {
		return $value;
	} else {
		foreach($layout['sub_fields'] as $sub) {
			if($sub['name'] == 'layout_title') {
				$key = $sub['key'];
				if(array_key_exists($i, $field['value']) && $value = $field['value'][$i][$key])
					return $value;
			}
		}
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title', 'my_layout_title', 10, 4);

// Floor Plans Custom Post type

function floor_plans_cpt() {
  register_post_type('floor_plans',
    array(
      'labels' => array(
        'name' => __('Floor Plans'),
        'singular_name' => __('Floor Plans'),
      ),
      'public' => true,
      'has_archive' => false,
      'menu_icon' => 'dashicons-align-center',
      'supports' => array('title'),
      'taxonomies' => array('category'),
    )
  );
}
add_action('init', 'floor_plans_cpt');

require_once( get_template_directory() . '/inc/functions-wordpress-seo.php' );
require_once( get_template_directory() . '/inc/functions-wordpress-seo-local.php' );
  
  
  
