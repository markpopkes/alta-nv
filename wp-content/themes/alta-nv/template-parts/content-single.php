<?php
/**
 * Template part for displaying single posts
 */
?>

<article <?php post_class( 'entry' ); ?>>
	<header class="entry__header">
		<?php the_title( '<h1 class="entry__title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry__content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
</article><!-- .entry -->
