<section id="section<?= get_row_index(); ?>" class="section" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
    <div class="grid-container fluid quote">
      <div class="grid-x">
        <div class="cell small-12 medium-12 large-12">
          <?php
            $tagline = get_sub_field('tagline');
            echo '<p>'.$tagline.'</p>'; ?>
        </div> <!-- .cell -->
      </div> <!-- .grid-x -->
    </div> <!-- .grid-container -->
  
    <div class="grid-container boxes">
      <div class="grid-x">
        <?php 
          $count = 1; 
          $sidebar_image = get_field('featured_image');
          $sidebar_large_headline = get_field('sidebar_large_headline');
          $sidebar_small_headline = get_field('sidebar_small_headline');
          $sidebar_button_text = get_field('sidebar_button_text');
          $sidebar_button_link = get_field('sidebar_button_link');

          if (have_rows('content_boxes')): while (have_rows('content_boxes')): the_row(); 
            $bg_image = get_sub_field('background_image'); 
            $title = get_sub_field('headline'); 
            $copy = get_sub_field('content'); 
            $bg_image = get_sub_field('background_image'); ?>
          <div class="cell small-12 medium-4 large-3 box box<?=$count;?>" style="<?= (!empty($bg_image) ? 'background-image:url('.$bg_image["url"].')' : ''); ?>">
            <div class="textarea">
              <h3><?php echo $title; ?></h3>
              <?= $copy; ?>
            </div> <!-- .textarea -->
          </div> <!-- .cell -->
        <?php $count++; endwhile; endif; ?>
        <div class="cell small-12 medium-3 large-3 box4"></div>
      </div> <!-- .grid-x -->
    </div> <!-- .grid-container -->

    <div id="sidebar" class="sidebar">
      <div class="sidebar-fixed">
        <img src="<?= (!empty($sidebar_image) ? $sidebar_image['url'] : ''); ?>" alt="Alta NV" />
        <div class="textbox-sidebar">
          <h3><?= $sidebar_large_headline; ?></h3>
          <p><?= $sidebar_small_headline; ?></p>
        </div> <!-- .textbox -->

        <div id="cta-sidebar">
          <a href="<?= $sidebar_button_link; ?>" class="cta-button" title="Reserve Now"><?= $sidebar_button_text; ?></a>
        </div> <!-- .cta-sidebar -->
        <span class="heroTab">Text Us</span>
      </div> <!-- .sidebar-fixed -->
    </div> <!-- #sidebar -->
  </section>