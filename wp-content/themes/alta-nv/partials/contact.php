<section id="section<?= get_row_index(); ?>" class="section grid-container full contact-form">
    <div class="grid-x">
      <div class="cell small-12 large-6">
        <div id="contact-form">
          <div class="contact-form__title">
            <h2>Get In Touch</h2>
            <div id="social">
              <a class="facebook" href="https://facebook.com" target="_blank" style="visibility:hidden"></a>
              <a class="instagram" href="https://instagram.com" target="_blank" style="visibility:hidden"></a>
            </div> <!-- #social -->
          </div> <!-- .contact-form__title -->
          <p>What will you discover while living a lifestyle worthy of NV? Please submit your information below to receive more information about living at Alta NV.</p>
          
          <div id="cta">
            <!--<a href="/contact/" class="cta-button" title="Schedule a Tour">Get In Touch</a>-->
          </div> <!-- .cta-sidebar -->

          <div class="home-contact">
            <?= do_shortcode('[gravityform id="4" title="false" ajax="true"]'); ?>
          </div> <!-- .home-contact -->
        </div> <!-- #contact-form -->
      </div>

      <div class="cell small-12 large-6">
        <div class="mapHolder">
          <div id="map" style="width:100%"></div>
        </div> <!-- .mapHolder -->
      </div>
    </div> <!-- .grid-x -->
  </section>