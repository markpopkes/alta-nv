# Starter Theme

This theme uses [Gulp][0] as a task runner to build and compile assets. [Sass][1] is the CSS preprocessor of choice.

These are the key facts to know when developing using this theme:

- Build tasks are configured to use [Gulp][0] as the task runner
- [Sass][1] is used to write and compile CSS
- The CSS grid is [Bulma][2] which uses [flexbox][3]
- Media query mixins are provided by [Bulma][5]
- Various utility classes are provided by [Bulma][12]
- A few mixins (mostly responsive font resizing) are used from [Rucksack][4]
- [Autoprefixer][6] is utilized to manage vendor prefixing

## Coding Standards

Whitespace is extremely important so ensure your editor is setup to use the suggested [Editor Config Settings][6].
- Soft tabs, 2 spaces for JS and Sass
- Hard tabs, 2 spaces for PHP

We also recommend soft tabs and to trim trailing whitespace on save (easily done in good editors like [VSCode][8]).

The [WordPress Coding Standards][9] has more information on formatting PHP, JavaScript and CSS source code. A responsible developer should ensure their contributions are in compliance with the community guidelines. Most of all though, consistency is key to writing good code.

## Development Environment

To get started, ensure the following is completed:

> _Note: You may need to preface the commands with `sudo` to avoid file permission errors._

1. Install [Node.js][10]
2. Install [Gulp][0]

## Prepare Theme Environment

To set up the local environment, run the following commands in the root of your local theme directory:

1. Move into the theme folder (file structure assumes [Local by Flywheel][11] usage)
`$ cd ~/Sites/site-name/app/public/wp-content/themes/<theme_name>`

2. Install Node packages
`$ npm install`

## Gulp Tasks

1. Run Sass task which compiles CSS into /dist folder
`$ gulp sass`

2. Run JS task which compiles JS into /dist folder
`$ gulp js`

3. Run Build task which compiles both CSS and JS into /dist folder
`$ gulp build`

4. Run Sprite task which takes svg files and creates a sprite.svg in /dist folder
`$ gulp sprite`

5. Run Watch task to compiles both CSS and JS automatically on file save
`$ gulp watch`

6. Run Default task (Build task)
`$ gulp`

## Cheatsheet

* Add Node Module
`$ npm install <package_name> --save-dev`

* Remove Node Module
`$ npm uninstall <package_name> --save-dev`

[0]: http://gulpjs.com/
[1]: https://sass-lang.com/
[2]: https://bulma.io/
[3]: https://css-tricks.com/snippets/css/a-guide-to-flexbox/
[4]: https://github.com/seaneking/rucksack
[5]: https://bulma.io/documentation/overview/responsiveness/
[6]: https://github.com/postcss/autoprefixer
[7]: http://editorconfig.org/
[8]: https://code.visualstudio.com/
[9]: https://codex.wordpress.org/WordPress_Coding_Standards
[10]: https://nodejs.org/download
[11]: https://localbyflywheel.com/
[12]: https://bulma.io/documentation/modifiers/syntax/
