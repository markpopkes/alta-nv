<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="stylesheet" href="https://use.typekit.net/hks5aap.css">
	<?php wp_head(); ?>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-NQQNJS6');</script>

  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-PFWJTK4');</script>
</head>

<body <?php if (get_body_class()) body_class(); ?>>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQQNJS6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<div id="promo-bar">
  <div id="promo">
    <h3><?php the_field('large_heading','option'); ?></h3>
    <h4><?php the_field('small_heading','option'); ?></h4>
    <div class="promo__close">
      <div class="promo-link js-promo-toggle">
        <div class="promo-link__outer">
          <div class="promo-link__icon"></div>
        </div> <!-- .promo-link__outer -->
      </div> <!-- .promo-link -->
    </div> <!-- .promo__close -->
  </div> <!-- #promo -->
</div> <!-- #promo-bar -->

<div id="wrapper">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell small-12 medium-12 large-12">
        <header id="header">
          <div class="left">
            <div id="logo">
              <?php 
                $logo_image = get_field('logo_image','option'); 
                $logo_image_hover = get_field('logo_image_hover','option'); ?>
              <a href="<?= home_url(); ?>" title="Alta NV">
                <img src="<?= $logo_image['url']; ?>" alt="AltaNV Logo" />
                <img class="hidden" src="<?= $logo_image_hover['url']; ?>" alt="AltaNV Logo" />
              </a>
            </div> <!-- #logo -->

            <div id="access">
              <?php 
                $link1 = get_field('fixed_header_menu_link_left','option');
                $link2 = get_field('fixed_header_menu_link_center','option');
                $link3 = get_field('fixed_header_menu_link_right','option'); ?>

              <a href="<?= (!empty($link1['url']) ? $link1['url'] : ''); ?>" ><?= (!empty($link1['title']) ? $link1['title'] : ''); ?></a>
              <a target="_blank" href="<?= (!empty($link2['url']) ? $link2['url'] : ''); ?>"><?= (!empty($link2['title']) ? $link2['title'] : ''); ?></a>
              <!--<a href="<?= (!empty($link3['url']) ? $link3['url'] : ''); ?>"><?= (!empty($link3['title']) ? $link3['title'] : ''); ?></a>-->

              <nav class="header__nav section">
                <div class="container">

                  <?php wp_nav_menu(array('theme_location' => 'menu-header','depth' => 1)); ?>

                  <div id="cta">
                    <a href="/contact/#section2" class="cta-button">Schedule a Tour</a>
                  </div>

                  <div id="canvas-links">
                    <?php wp_nav_menu(array('theme_location' => 'menu-nav-links','depth' => 1)); ?>
                  </div> <!-- #canvas-links -->

                  <div id="canvas-social" style="visibility:hidden">
                    <a href="https://facebook.com" target="_blank" class=" dashicons dashicons-facebook"></a>
                    <a href="https://google.com" target="_blank" class="dashicons dashicons-instagram"></a>
                  </div> <!-- .canvas-social -->

                </div> <!-- .container -->
              </nav> <!-- .header__nav -->

              <div class="nav-link js-nav-toggle">
                <div class="nav-link__outer">
                  <div class="nav-link__icon"></div>
                </div> <!-- .nav-link -->
              </div> <!-- .nav-link -->
            </div> <!-- #access -->

            <div id="header-bg"></div>
          </div> <!-- .left -->

          <div class="right">
            <div id="cta-header">
              <?php
                $cta_button = get_field('cta_button','option'); ?>
                  <a href="<?= $cta_button['url']; ?>" class="cta-button <?= is_front_page() ? 'hidden': ''; ?>"><?= $cta_button['title']; ?></a>
            </div> <!-- #cta-header -->
          </div> <!-- .right -->
        </header> <!-- #header -->
        
        <main role="main">