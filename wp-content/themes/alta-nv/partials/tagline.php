<?php
  $tagline = get_sub_field('tagline'); 
  $bg_color = get_sub_field('bg_color'); ?>
<section id="section<?= get_row_index(); ?>" class="section tagline" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
  <div class="grid-container fluid quote">
  <div class="grid-x">
      <div class="cell small-12 medium-12 large-12">
        <p class="tag-text"><?= $tagline; ?></p>
      </div> <!-- .cell -->
  </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>